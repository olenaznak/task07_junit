import org.junit.*;

import static org.junit.Assert.*;

public class CalculatorTest {
    private Calculator math;

    @Before
    public void init() {
        math = new Calculator();
        System.out.println("Before");
    }
    @After
    public void tearDown() {
        math = null;
        System.out.println("After");
    }

    @Test
    public void testCalls() {
        assertEquals(0, math.getCalls());

        math.calculateFactorial(1);
        assertEquals(1, math.getCalls());

        math.calculateFactorial(1);
        assertEquals(2, math.getCalls());
    }

    @Test
    public void testSubtractAndAdd() {
        assertNotSame(math.add(2, 3), math.subtract(2,3));
       // assertSame(math.add(0,0), math.subtract(0,0));
    }

    @Test
    public void testFactorial() {
        assertTrue(math.calculateFactorial(0) == 1);
        assertTrue(math.calculateFactorial(1) == 1);
        assertTrue(math.calculateFactorial(5) == 120);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeFactorial() {
        math.calculateFactorial(-1);
    }

    @Ignore
    @Test
    public void ignoredTest() {
        assertEquals(math.add(1, 1), 3);
    }
}
