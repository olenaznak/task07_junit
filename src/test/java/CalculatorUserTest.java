import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class CalculatorUserTest {

    @Mock
    ICalculator calc;

    @InjectMocks
    CalculatorUser calcUser = new CalculatorUser();

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Before Class");
    }

   @AfterClass
    public static void afterClass() {
        System.out.println("After Class");
    }

    @Test
    public void testAddNums() {
        when(calc.add(3, 2)).thenReturn(5);
        when(calc.add(6, 7)).thenReturn(13);
        assertEquals(calcUser.addNums(3, 2), 5);
        assertEquals(calcUser.addNums(6,7), 13);
    }

    @Test
    public void testAddNumberToFactorial() {
        when(calc.calculateFactorial(3)).thenReturn(6L);
        when(calc.calculateFactorial(4)).thenReturn(24L);
        assertEquals(calcUser.addNumberToFactorial(3, 4), 10L);
        assertEquals(calcUser.addNumberToFactorial(4,-7), 17);
    }

    @Test
    public void testWriteFile() {
        String str = "E:\\test.txt";
        calcUser.writeFile(str);
    }


}
