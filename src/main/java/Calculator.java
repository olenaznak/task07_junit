public class Calculator {
    int calls;

    public int getCalls() {
        return calls;
    }

    public long calculateFactorial(int number) {
        calls++;

        if (number < 0)
            throw new IllegalArgumentException();

        long result = 1;
        if (number > 1) {
            for (int i = 1; i <= number; i++)
                result = result * i;
        }
        return result;
    }

    public int add(int num1, int num2) {
        calls++;
        return num1 + num2;
    }

    public long subtract(int num1, int num2) {
        calls++;
        return num1 - num2;
    }
}
