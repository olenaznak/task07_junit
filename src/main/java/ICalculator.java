public interface ICalculator {
    public int getCalls();

    public long calculateFactorial(int number);

    public int add(int num1, int num2);

    public long subtract(int num1, int num2);
}
