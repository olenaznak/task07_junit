import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class CalculatorUser {

    private Calculator calculator = new Calculator();

    final static int MAX_PERCENT = 100;

    public long addNumberToFactorial(int factorialNumber, int number) {
        return calculator.calculateFactorial(factorialNumber) + number;
    }

    public long addNums(int a, int b) {
        return calculator.add(a, b);
    }

    public void writeFile(String name) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(name))) {
            bw.write("Calculator works");
        } catch (IOException exc) {
            System.out.println("Помилка запису");
        }
    }

    public boolean isAppropriatePercent(int percent) {
        if(percent > MAX_PERCENT) {
            return false;
        }
        return true;
    }


}